import rewire from 'rewire'

const app = rewire('./code.js');
const fn = app.__get__('fn');

describe("User's code", () => {


    it('should do smth right', () => {
        expect(fn(2)).toEqual(4);
        expect(fn(4)).toEqual(6);
    });
});
